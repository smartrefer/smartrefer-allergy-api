import fp from "fastify-plugin"
const Knex = require("knex")

module.exports = fp(async (fastify: any, opts: any, done: any) => {

  try {
    const handler = await Knex(opts.options);
    fastify.decorate(opts.connectionName, handler);
    done();
  } catch (err) {
    done(err);
  }

})
import { FastifyInstance } from 'fastify';

import indexRoute from './controllers/index'
export default async (fastify: FastifyInstance) => {
  fastify.register(require('./controllers/index'), { prefix: '/' });
  fastify.register(require('./controllers/services.controllers'), { prefix: '/services' });
}

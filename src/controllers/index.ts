import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify"

export default async (fastify: FastifyInstance) => {

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'Cashier  -->Fastify, RESTful API services!!!!!! 20220204' });
  });

  // fastify.get("/jwt/sign", async (request: FastifyRequest, reply: FastifyReply) => {
  //   const payload: any = { userId: 1, fullname: "Demo Demo" }
  //   const accessToken = fastify.jwt.sign(payload)
  //   reply.send({ accessToken })
  // })

  // fastify.get("/jwt/verify", {
  //   preValidation: [fastify.authenticate]
  // }, (request: FastifyRequest, reply: FastifyReply) => {
  //   const decoded: any = request.user
  //   reply.send({ message: "Protected area!", decoded })
  // })

}

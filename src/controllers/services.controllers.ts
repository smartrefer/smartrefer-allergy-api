import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify"
import moment from 'moment-timezone';
import { SendAllergyModel } from '../models/ket/api_allergy.model';
var cron = require('node-cron');
const number = Math.floor(Math.random() * 60);

// His Model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'


export default async (fastify: FastifyInstance) => {

    // ห้ามแก้ไข // 
    //-----------------BEGIN START-------------------//
    const provider = process.env.HIS_PROVIDER;
    const connection = process.env.DB_CLIENT;
    const sendAllergyModel = new SendAllergyModel()
    let db: any;
    let hisModel: any;

    switch (connection) {
        case 'mysql2':
            db = fastify.mysql2;
            break;
        case 'mysql':
            db = fastify.mysql;
            break;
        case 'mssql':
            db = fastify.mssql;
            break;
        case 'pg':
            db = fastify.pg;
            break;
        default:
    }

    switch (provider) {
        case 'ubase':
            hisModel = new HisUbaseModel();
            break;
        case 'himpro':
            hisModel = new HisHimproHiModel();
            break;
        case 'jhcis':
            hisModel = new HisJhcisHiModel();
            break;
        case 'hosxpv3':
            hisModel = new HisHosxpv3Model();
            break;
        case 'hosxpv3arjaro':
            hisModel = new HisHosxpv3ArjaroModel();
            break;
        case 'hosxpv4':
            hisModel = new HisHosxpv4Model();
            break;
        case 'hosxpv4pg':
            hisModel = new HisHosxpv4PgModel();
            break;
        case 'hi':
            hisModel = new HisHiModel();
            break;
        case 'homc':
            hisModel = new HisHomcModel();
            break;
        case 'mbase':
            hisModel = new HisMbaseModel();
            break;
        case 'phosp':
            hisModel = new HisPhospModel();
            break;
        case 'universal':
            hisModel = new HisUniversalHiModel();
            break;
        case 'sakon':
            hisModel = new HisSakonModel();
            break;
        case 'homcudon':
            hisModel = new HisHomcUbonModel();
            break;
        case 'softcon':
            hisModel = new HisSoftConModel();
            break;
        case 'softconudon':
            hisModel = new HisSoftConUdonModel();
            break;
        case 'ssb':
            hisModel = new HisSsbModel();
            break;
        case 'panaceaplus':
            hisModel = new HisPanaceaplusModel();
            break;
        default:
        // hisModel = new HisModel();
    }
    //------------------BEGIN END------------------//


    fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
        reply.code(200).send({ message: 'Services  -->Fastify, RESTful API services!!!!!! 20220204' });
    });


    fastify.post('/view/person', async (request: FastifyRequest, reply: FastifyReply) => {        
        try {

            let rs_person:any = await hisModel.getPerson(db);
    
            let info:any = {
                person:rs_person,
            }
            
            if(info.person[0]){
                await sendAllergyModel.person(info.person)
                reply.send({ok:true,rows:info.person});
            }else{
                reply.send({ok:false,rows:info.person});
            }
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }


    });

    fastify.post('/view/allergy', async (request: FastifyRequest, reply: FastifyReply) => {
        console.log("view");
        
        try {

            let rs_allergy:any = await hisModel.getAllergy(db);

            let info:any = {
                allergy:rs_allergy,
            }
            if(info.allergy[0]){
                await sendAllergyModel.allergy(info.allergy)
                reply.send({ok:true,rows:info.allergy});
            }else{
                reply.send({ok:false,rows:info.allergy});
            }            
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }


    });

    fastify.post('/view/drugs', async (request: FastifyRequest, reply: FastifyReply) => {
        console.log("view");
        
        try {

            let rs_drugs:any = await hisModel.getDrugs(db);
    
            let info:any = {
                drugs:rs_drugs,
            }
            if(info.drugs[0]){
                await sendAllergyModel.drugs(info.drugs)
                reply.send({ok:true,rows:info.drugs});
            }else{
                reply.send({ok:false,rows:info.drugs});
            }               
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }


    });


    fastify.post('/view/g6pd', async (request: FastifyRequest, reply: FastifyReply) => {
        console.log("view");
        
        try {

            let rs_g6pd:any = await hisModel.getG6pd(db);
            let info:any = {
                g6pd:rs_g6pd
            }
            if(info.g6pd[0]){
                console.log(info.g6pd);
                
                let rs_x = await sendAllergyModel.g6pd(info.g6pd)
                console.log(rs_x);
                
                reply.send({ok:true,rows:info.g6pd});
            }else{
                reply.send({ok:false,rows:info.g6pd});
            }                           
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

    fastify.post('/view/today', async (request: FastifyRequest, reply: FastifyReply) => {
        console.log("view");
        
        try {

            let rs_person:any = await hisModel.getPersonToday(db);
            let rs_allergy:any = await hisModel.getAllergyToday(db);
            let rs_drugs:any = await hisModel.getDrugsToday(db);
            let rs_g6pd:any = await hisModel.getG6pdToday(db);
    
            let info:any = {
                person:rs_person,
                allergy:rs_allergy,
                drugs:rs_drugs,
                g6pd:rs_g6pd
            }
            if(info.person[0]){                
                let rs_x = await sendAllergyModel.person(info.person)
            }    
            if(info.allergy[0]){                
                let rs_x = await sendAllergyModel.allergy(info.allergy)
            }    
            if(info.drugs[0]){                
                let rs_x = await sendAllergyModel.drugs(info.drugs)
            }    
            if(info.g6pd[0]){                
                let rs_x = await sendAllergyModel.g6pd(info.g6pd)
            }    

            await reply.send({ok:true,rows:info});                      
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    });

    fastify.post('/view/date', async (request: FastifyRequest, reply: FastifyReply) => {
        console.log("view");
        const req:any = request;
        const datestart:any = req.body.datestart;
        const dateend:any = req.body.dateend;
        
        try {

            let rs_person:any = await hisModel.getPersonDate(db,datestart,dateend);
            let rs_allergy:any = await hisModel.getAllergyDate(db,datestart,dateend);
            let rs_drugs:any = await hisModel.getDrugsDate(db,datestart,dateend);
    
            let info:any = {
                person:rs_person,
                allergy:rs_allergy,
                drugs:rs_drugs,
            }
            if(info.person[0]){                
                let rs_x = await sendAllergyModel.person(info.person)
            }    
            if(info.allergy[0]){                
                let rs_x = await sendAllergyModel.allergy(info.allergy)
            }    
            if(info.drugs[0]){                
                let rs_x = await sendAllergyModel.drugs(info.drugs)
            }    

            await reply.send({ok:true,rows:info});                      
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    });

    cron.schedule(`${number} 10 * * *`, async (req: FastifyRequest, reply: FastifyReply) => {
        console.log(`running a task every minute 1 : ${number}`);
        try {

            let rs_person:any = await hisModel.getPersonToday(db);
            let rs_allergy:any = await hisModel.getAllergyToday(db);
            let rs_drugs:any = await hisModel.getDrugsToday(db);
            let rs_g6pd:any = await hisModel.getG6pdToday(db);
    
            let info:any = {
                person:rs_person,
                allergy:rs_allergy,
                drugs:rs_drugs,
                g6pd:rs_g6pd
            }
            if(info.person[0]){                
                let rs_x = await sendAllergyModel.person(info.person)
                // console.log(rs_x);
            }    
            if(info.allergy[0]){                
                let rs_x = await sendAllergyModel.allergy(info.allergy)
                // console.log(rs_x);
            }    
            if(info.drugs[0]){                
                let rs_x = await sendAllergyModel.drugs(info.drugs)
                // console.log(rs_x);
            }    
            if(info.g6pd[0]){                
                let rs_x = await sendAllergyModel.g6pd(info.g6pd)
                // console.log(rs_x);
            }    

            await reply.send({ok:true,rows:info});                      
    
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}

import * as fastify from "fastify"
import routers from "./router"
import { join } from "path"

const multer = require("fastify-multer")

require("dotenv").config({ path: join(__dirname, "../config.conf") })

const app: fastify.FastifyInstance = fastify.fastify({
  logger: { level: "info" }
})

app.register(multer.contentParser)

app.register(require("@fastify/formbody"))
app.register(require("@fastify/cors"), {})
// First connection
//  Connection
if(process.env.DB_CLIENT == 'mssql'){
  app.register(require('./plugins/db'), {

    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        trustServerCertificate: true
      },
      pool: {
        min: 0,
        max: Number(process.env.DB_MAX_CONNECTION)
      },
      debug: process.env.DB_DEBUG === "Y" ? true : false,
      
    }
  })
}else if(process.env.DB_CLIENT == 'pg'){
  app.register(require('./plugins/db'), {

    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
      },
      pool: {
        min: 0,
        max: Number(process.env.DB_MAX_CONNECTION)
      },
      debug: process.env.DB_DEBUG === "Y" ? true : false,
    }
  })
}else{
  app.register(require('./plugins/db'), {

    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
      },
      pool: {
        min: 0,
        max: Number(process.env.DB_MAX_CONNECTION),
        afterCreate: (conn:any, done:any) => {
          conn.query('SET NAMES ' + process.env.DB_CHARSET, (err:any) => {
            done(err, conn);
          });
        },
      },
      debug: process.env.DB_DEBUG === "Y" ? true : false,
    }
  })
}

app.register(require("./plugins/jwt"), {
  secret: process.env.SECRET_KEY || "@1%098awbder90@",
  aud: "domain.ltd",
  iss: "domain.ltd"
})

// Axios
app.register(require("fastify-axios"), {
  clients: {
    v1: {
      baseURL: "https://apingweb.com/api/rest",
    },
    v2: {
      baseURL: "https://randomuser.me/api"
    }
  }
})

// QR Code
app.register(require("@chonla/fastify-qrcode"))

app.ready(err => {
  if (err) throw err
})

app.register(routers)

export default app;
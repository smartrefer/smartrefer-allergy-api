import { Knex } from 'knex';

export class HisHomcModel {


    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        select DISTINCT 
            '10669' AS 'hospital_code' , 
            psx.CardID  as 'cid' ,
            LTRIM(RTRIM( ptt.titleName )) AS 'title' , 
            ptx.firstName  AS 'fname'  ,  
            ptx.lastName AS 'lname'
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN PTITLE ptt (nolock) ON ptt.titleCode = ptx.titleCode
        WHERE ( psx.CardID <> '' OR psx.CardID is not NULL )  
        AND ( LTRIM(RTRIM( ptx.firstName )) <> '' AND LTRIM(RTRIM( ptx.firstName )) IS NOT NULL )
        AND ( LTRIM(RTRIM( ptx.lastName )) <> '' AND LTRIM(RTRIM( ptx.lastName )) IS NOT NULL )
        `);
        return data;
    }
    
    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        select 
            '10669' as 'hospital_code'  ,
            Ltrim(rtrim( vx.name )) as 'drug_name' , 
            mcs.Map_SM_Refer as 'allergy_level_id'  , 
            dbo.ymd2ce( dbo.ce2ymd( rx.updDate ) ) as 'report_date' , 
            vx.TMT  as  'tmt_id' ,  
            mx.invGovCode24 as 'std_code' , 
            rx.alergyNote as 'symptom'  ,  
            psx.CardID as 'cid' 
        from medalery rx           
        left join Med_inv vx (nolock) on vx.code = rx.medCode  
        left join PATIENT ptx (nolock) on ptx.hn = rx.hn 
        left join PatSS   psx (nolock) on psx.hn = rx.hn         
        left join Med_Gov24Map mx (nolock) on mx.invCode = rx.medCode  
        left join MedCausality mcs (nolock) on mcs.causalityCode = rx.causality
        where rx.alergyType <> 'G6PD'         
        order by rx.updDate desc
        `);
        return data;
    }
    
    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        select DISTINCT
            '10669' AS 'hospital_code' , 
            Ltrim(Rtrim(vx.name)) AS 'drug_name' , 
            vx.TMT AS 'tmt_id' ,
            mx.invGovCode24 as 'std_code'  ,
            ( SELECT TOP 1 px2.lastIssTime AS 'use_date' FROM Patmed px2 WHERE px2.hn = ptx.hn  ORDER BY  px2.lastIssTime DESC  ) AS 'use_date',
            psx.CardID  as 'cid' 
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN Patmed pmx (nolock) on ptx.hn = pmx.hn 
        LEFT JOIN Med_inv vx (nolock) ON vx.code = pmx.invCode 
        left join Med_Gov24Map mx (nolock) on mx.invCode = pmx.invCode 
        WHERE vx.GPID IN  ('219068')   AND  ( psx.CardID <> '' OR psx.CardID is not NULL )
        GROUP BY ptx.hn , vx.name  , vx.TMT  ,  psx.CardID ,  mx.invGovCode24 
        HAVING SUM( pmx.accQty ) > 0
        `);
        return data;
    }
    
    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        select DISTINCT  psx.CardID as 'cid'   
        from medalery rx           
        left join Med_inv vx (nolock) on vx.code = rx.medCode  
        left join PATIENT ptx (nolock) on ptx.hn = rx.hn 
        left join PatSS   psx (nolock) on psx.hn = rx.hn         
        where  rx.alergyType = 'G6PD'
        `);
        return data;
    }
    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        select 
            '10669' AS 'hospital_code' ,
            psx.CardID  as 'cid' ,
            LTRIM(RTRIM( ptt.titleName )) AS 'title' , 
            ptx.firstName  AS 'fname'  ,  
            ptx.lastName AS 'lname'
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN PTITLE ptt (nolock) ON ptt.titleCode = ptx.titleCode
        WHERE ( psx.CardID <> '' OR psx.CardID is not NULL )  
        AND EXISTS ( SELECT * FROM  medalery mxd WHERE mxd.hn = ptx.hn  AND dbo.ce2ymd( mxd.updTime ) = dbo.ce2ymd( GETDATE() - 1 )    )
        AND ( LTRIM(RTRIM( ptx.firstName )) <> '' AND LTRIM(RTRIM( ptx.firstName )) IS NOT NULL )
        AND ( LTRIM(RTRIM( ptx.lastName )) <> '' AND LTRIM(RTRIM( ptx.lastName )) IS NOT NULL )    ;
        `);
        return data;
    }
    
    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        select 
            '10669' as 'hospital_code' ,
            Ltrim(rtrim( vx.name )) as 'drug_name' , 
            mcs.Map_SM_Refer as 'allergy_level_id'  , 
            dbo.ymd2ce( dbo.ce2ymd( rx.updDate ) ) as 'report_date' , 
            vx.TMT  as  'tmt_id' ,  
            mx.invGovCode24 as 'std_code' , 
            rx.alergyNote as 'symptom'  ,  
            psx.CardID as 'cid'    
        from medalery rx           
        left join Med_inv vx (nolock) on vx.code = rx.medCode  
        left join PATIENT ptx (nolock) on ptx.hn = rx.hn 
        left join PatSS   psx (nolock) on psx.hn = rx.hn         
        left join Med_Gov24Map mx (nolock) on mx.invCode = rx.medCode  
        left join MedCausality mcs (nolock) on mcs.causalityCode = rx.causality
        where dbo.ce2ymd( rx.updTime ) = dbo.ce2ymd( GETDATE() - 1 ) 
        and rx.alergyType <> 'G6PD'         
        order by rx.updDate desc  ;
        `);
        return data;
    }
    
    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        select DISTINCT 
            '10669' AS 'hospital_code' ,
            Ltrim(Rtrim(vx.name)) AS 'drug_name' ,
            vx.TMT AS 'tmt_id' ,
            mx.invGovCode24 as 'std_code'  ,
            ( SELECT TOP 1 px2.lastIssTime AS 'use_date' FROM Patmed px2 WHERE px2.hn = ptx.hn  ORDER BY  px2.lastIssTime DESC  ) AS 'use_date',
            psx.CardID  as 'cid' 
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN Patmed pmx (nolock) on ptx.hn = pmx.hn 
        LEFT JOIN Med_inv vx (nolock) ON vx.code = pmx.invCode 
        left join Med_Gov24Map mx (nolock) on mx.invCode = pmx.invCode 
        WHERE vx.GPID IN  ('219068')   AND  ( psx.CardID <> '' OR psx.CardID is not NULL )
        AND dbo.ce2ymd( pmx.lastIssTime ) = dbo.ce2ymd( GETDATE() - 1 ) 
        GROUP BY ptx.hn , vx.name  , vx.TMT  ,  psx.CardID ,  mx.invGovCode24 
        HAVING SUM( pmx.accQty ) > 0     ;
        `);
        return data;
    }
    
    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
        select DISTINCT  
            psx.CardID as 'cid'   
        from medalery rx           
        left join Med_inv vx (nolock) on vx.code = rx.medCode  
        left join PATIENT ptx (nolock) on ptx.hn = rx.hn 
        left join PatSS   psx (nolock) on psx.hn = rx.hn   
        where  rx.alergyType = 'G6PD'  
        AND  dbo.ce2ymd( rx.updTime ) = dbo.ce2ymd( GETDATE() - 1 ) 
        ;`);
        return data;
    }

    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
        select 
            '10669' AS 'hospital_code' ,
            psx.CardID  as 'cid' ,
            LTRIM(RTRIM( ptt.titleName )) AS 'title' , 
            ptx.firstName  AS 'fname'  ,  
            ptx.lastName AS 'lname'
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN PTITLE ptt (nolock) ON ptt.titleCode = ptx.titleCode
        WHERE ( psx.CardID <> '' OR psx.CardID is not NULL )  
        AND EXISTS ( SELECT * FROM  medalery mxd WHERE mxd.hn = ptx.hn  AND dbo.ce2ymd( mxd.updTime ) between '${datestart}' and '${dateend}'    )
        AND ( LTRIM(RTRIM( ptx.firstName )) <> '' AND LTRIM(RTRIM( ptx.firstName )) IS NOT NULL )
        AND ( LTRIM(RTRIM( ptx.lastName )) <> '' AND LTRIM(RTRIM( ptx.lastName )) IS NOT NULL )    ;
        `);
        return data;
    }
    
    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
        select 
            '10669' as 'hospital_code' ,
            Ltrim(rtrim( vx.name )) as 'drug_name' , 
            mcs.Map_SM_Refer as 'allergy_level_id'  , 
            dbo.ymd2ce( dbo.ce2ymd( rx.updDate ) ) as 'report_date' , 
            vx.TMT  as  'tmt_id' ,  
            mx.invGovCode24 as 'std_code' , 
            rx.alergyNote as 'symptom'  ,  
            psx.CardID as 'cid'    
        from medalery rx           
        left join Med_inv vx (nolock) on vx.code = rx.medCode  
        left join PATIENT ptx (nolock) on ptx.hn = rx.hn 
        left join PatSS   psx (nolock) on psx.hn = rx.hn         
        left join Med_Gov24Map mx (nolock) on mx.invCode = rx.medCode  
        left join MedCausality mcs (nolock) on mcs.causalityCode = rx.causality
        where dbo.ce2ymd( rx.updTime ) between '${datestart}' and '${dateend}'
        and rx.alergyType <> 'G6PD'         
        order by rx.updDate desc  ;
        `);
        return data;
    }
    
    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
        select DISTINCT 
            '10669' AS 'hospital_code' ,
            Ltrim(Rtrim(vx.name)) AS 'drug_name' ,
            vx.TMT AS 'tmt_id' ,
            mx.invGovCode24 as 'std_code'  ,
            ( SELECT TOP 1 px2.lastIssTime AS 'use_date' FROM Patmed px2 WHERE px2.hn = ptx.hn  ORDER BY  px2.lastIssTime DESC  ) AS 'use_date',
            psx.CardID  as 'cid' 
        from  PATIENT  ptx 
        LEFT JOIN PatSS psx (nolock) on psx.hn = ptx.hn 
        LEFT JOIN Patmed pmx (nolock) on ptx.hn = pmx.hn 
        LEFT JOIN Med_inv vx (nolock) ON vx.code = pmx.invCode 
        left join Med_Gov24Map mx (nolock) on mx.invCode = pmx.invCode 
        WHERE vx.GPID IN  ('219068')   AND  ( psx.CardID <> '' OR psx.CardID is not NULL )
        AND dbo.ce2ymd( pmx.lastIssTime ) between '${datestart}' and '${dateend}'
        GROUP BY ptx.hn , vx.name  , vx.TMT  ,  psx.CardID ,  mx.invGovCode24 
        HAVING SUM( pmx.accQty ) > 0     ;
        `);
        return data;
    }
}

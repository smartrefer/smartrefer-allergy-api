import { Knex } from 'knex';

export class HisJhcisHiModel {

    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(``);
        return data[0];
    }
    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(``);
        return data[0];
    }
    
    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(``);
        return data[0];
    }

}

import { Knex } from 'knex';

export class HisHosxpv4PgModel {


  async getPerson(db: Knex) {
    // ชื่อผู้ป่วย
    // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        cid as cid,
        pname as title,
        fname as fname,
        lname as lname
        from 
        patient
        where hn in (select DISTINCT on (hn) hn from opd_allergy)
    `);
    return data.rows;
}

async getAllergy(db: Knex) {
    // แพ้ยา
    // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        oa.agent::text as drug_name,
        oa.seriousness_id as allergy_level_id,
        oa.report_date::date as report_date,
        di.tmt_tp_code::text as tmt_id,
        oa.agent_code24::text as std_code,
        oa.symptom as symptom,
        pt.cid as cid
        from 
        opd_allergy oa
        left join patient pt on oa.hn = pt.hn
        left join (select * from drugitems where did <> '') di on oa.agent_code24 = di.did
    `);
    return data.rows;
}

async getDrugs(db: Knex) {
    // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
    let data = await db.raw(`
            select 
            (select hospitalcode from opdconfig)::text as hospital_code,
            di.name as drug_name,
            tmt.tpu_code as tmt_id,
            di.did as std_code,
            oi.rxdate as use_date,
            pt.cid as cid
        from 
            opitemrece oi
            inner join drugitems di on oi.icode = di.icode
            left join patient pt on oi.hn = pt.hn
            inner join tmt_tp_to_tpu tmt on di.tmt_tp_code = tmt.tp_code
        where tmt.tpu_code in (
        '208802',
        '208818',
        '208825',
        '219068',
        '219075',
        '219081',
        '196258',
        '196262',
        '196270',
        '196289',
        '107576',
        '107582',
        '107595',
        '107609'
        )
    `);
    return data.rows;
}

async getG6pd(db: Knex) {
    // return [{cid:''}]
    let data = await db.raw(`
        select DISTINCT on (p.cid)
        p.cid
        from 
        ovstdiag o
        inner join patient p on (replace(p.oldcode, ' ', '0')=o.hn or p.hn=o.hn)
        where
        icd10 like 'D55%'
    `);
    return data.rows;
}

async getPersonToday(db: Knex) {
    // ชื่อผู้ป่วย
    // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        cid as cid,
        pname as title,
        fname as fname,
        lname as lname
        from 
        patient
        where hn in (select DISTINCT on (hn) hn from opd_allergy where report_date = CURRENT_DATE - 1)
    `);
    return data.rows;
}

async getAllergyToday(db: Knex) {
    // แพ้ยา
    // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        oa.agent::text as drug_name,
        oa.seriousness_id as allergy_level_id,
        oa.report_date::date as report_date,
        di.tmt_tp_code::text as tmt_id,
        oa.agent_code24::text as std_code,
        oa.symptom as symptom,
        pt.cid as cid
        from 
        opd_allergy oa
        left join patient pt on oa.hn = pt.hn
        left join (select * from drugitems where did <> '') di on oa.agent_code24 = di.did
        where
        oa.report_date = CURRENT_DATE - 1
    `);
    return data.rows;
}

async getDrugsToday(db: Knex) {
    // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
    let data = await db.raw(`
            select 
            (select hospitalcode from opdconfig)::text as hospital_code,
            di.name as drug_name,
            tmt.tpu_code as tmt_id,
            di.did as std_code,
            oi.rxdate as use_date,
            pt.cid as cid
        from 
            opitemrece oi
            inner join drugitems di on oi.icode = di.icode
            left join patient pt on oi.hn = pt.hn
            inner join tmt_tp_to_tpu tmt on di.tmt_tp_code = tmt.tp_code
        where tmt.tpu_code in (
        '208802',
        '208818',
        '208825',
        '219068',
        '219075',
        '219081',
        '196258',
        '196262',
        '196270',
        '196289',
        '107576',
        '107582',
        '107595',
        '107609'
        ) and date(oi.rxdate) = CURRENT_DATE - 1
    `);
    return data.rows;
}

async getG6pdToday(db: Knex) {
    // return [{cid:''}]
    let data = await db.raw(`
        select DISTINCT on (p.cid)
        p.cid
        from 
        ovstdiag o
        inner join patient p on (replace(p.oldcode, ' ', '0')=o.hn or p.hn=o.hn)
        where
        icd10 like 'D55%'
    `);
    return data.rows;
}

async getPersonDate(db: Knex,datestart:any,dateend:any) {
    // ชื่อผู้ป่วย
    // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        cid as cid,
        pname as title,
        fname as fname,
        lname as lname
        from 
        patient
        where hn in (select DISTINCT on (hn) hn from opd_allergy where date(report_date) between '${datestart}' and '${dateend}')
    `);
    return data.rows;
}

async getAllergyDate(db: Knex,datestart:any,dateend:any) {
    // แพ้ยา
    // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
    let data = await db.raw(`
        select 
        (select hospitalcode from opdconfig)::text as hospital_code,
        oa.agent::text as drug_name,
        oa.seriousness_id as allergy_level_id,
        oa.report_date::date as report_date,
        di.tmt_tp_code::text as tmt_id,
        oa.agent_code24::text as std_code,
        oa.symptom as symptom,
        pt.cid as cid
        from 
        opd_allergy oa
        left join patient pt on oa.hn = pt.hn
        left join (select * from drugitems where did <> '') di on oa.agent_code24 = di.did
        where
        date(oa.report_date) between '${datestart}' and '${dateend}'
    `);
    return data.rows;
}

async getDrugsDate(db: Knex,datestart:any,dateend:any) {
    // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
    let data = await db.raw(`
            select 
            (select hospitalcode from opdconfig)::text as hospital_code,
            di.name as drug_name,
            tmt.tpu_code as tmt_id,
            di.did as std_code,
            oi.rxdate as use_date,
            pt.cid as cid
        from 
            opitemrece oi
            inner join drugitems di on oi.icode = di.icode
            left join patient pt on oi.hn = pt.hn
            inner join tmt_tp_to_tpu tmt on di.tmt_tp_code = tmt.tp_code
        where tmt.tpu_code in (
        '208802',
        '208818',
        '208825',
        '219068',
        '219075',
        '219081',
        '196258',
        '196262',
        '196270',
        '196289',
        '107576',
        '107582',
        '107595',
        '107609'
        ) and date(oi.rxdate) between '${datestart}' and '${dateend}'
    `);
    return data.rows;
}


}

import { Knex } from 'knex';

export class HisHiModel {


    async getPerson(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
            SELECT DISTINCT * FROM (select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join allergy on pt.hn = allergy.hn 
            group by pt.hn
            union 
            select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn=ovst.vn 
            inner join prscdt on prscdt.prscno=prsc.prscno 
            inner join meditem on prscdt.meditem=meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609
            )
            group by pt.hn
            union 
            select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join persong6pd on pt.hn = persong6pd.hn
            group by pt.hn) as p 
        `) ;
        return data[0];
    }

    async getAllergy(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            allergy.namedrug as drug_name,
            (case allergy.allgtype when '1' then 1 when '2' then 2 when '3' then 3 when '4' then 3 else 5 end) as allergy_level_id,
            allergy.entrydate as report_date,
            meditem.tmtid tmt_id,
            meditem.stdcode as std_code,
            allergy.detail as symptom,
            pt.pop_id as cid
            from 
            pt
            inner join 
            allergy on pt.hn=allergy.hn
            left join 
            meditem on allergy.meditem = meditem.meditem
            where pt.dthdate = '0000-00-00'
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
        `);
        return data[0];
    }

    async getDrugs(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            meditem.pres_nm as drug_name,
            meditem.tmtid as tmt_id,
            meditem.stdcode as std_code,
            prsc.prscdate as use_date,
            pt.pop_id as cid
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn = ovst.vn 
            inner join prscdt on prscdt.prscno = prsc.prscno 
            inner join meditem on prscdt.meditem = meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609)
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
        `);
        return data[0];
    }

    async getG6pd(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
            select 
            pt.pop_id as cid
            from hi.pt
            inner join hi.persong6pd on pt.hn = persong6pd.hn
            where pt.dthdate = '0000-00-00'
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            group by pt.hn
        `);
        return data[0];
    }

    async getPersonToday(db: Knex) {
        // ชื่อผู้ป่วย
        // return [{hospital_code:'',cid:'',title:'',fname:'',lname:''}]
        let data = await db.raw(`
            SELECT DISTINCT * FROM (select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join allergy on pt.hn = allergy.hn 
            where allergy.entrydate = CURRENT_DATE - 1
            group by pt.hn
            union 
            select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn=ovst.vn 
            inner join prscdt on prscdt.prscno=prsc.prscno 
            inner join meditem on prscdt.meditem=meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609
            )
            and date(vstdttm) = CURRENT_DATE - 1
            group by pt.hn
            union 
            select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join persong6pd on pt.hn = persong6pd.hn
            group by pt.hn) as p 
        `) ;
        return data[0];
    }

    async getAllergyToday(db: Knex) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            allergy.namedrug as drug_name,
            allergy.allgtype as allergy_level_id,
            allergy.entrydate as report_date,
            meditem.tmtid tmt_id,
            meditem.stdcode as std_code,
            allergy.detail as symptom,
            pt.pop_id as cid
            from 
            pt
            inner join 
            allergy on pt.hn=allergy.hn
            left join 
            meditem on allergy.meditem = meditem.meditem
            where pt.dthdate = '0000-00-00'
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            and allergy.entrydate = CURRENT_DATE - 1
        `);
        return data[0];
    }

    async getDrugsToday(db: Knex) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            meditem.pres_nm as drug_name,
            meditem.tmtid as tmt_id,
            meditem.stdcode as std_code,
            prsc.prscdate as use_date,
            pt.pop_id as cid
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn = ovst.vn 
            inner join prscdt on prscdt.prscno = prsc.prscno 
            inner join meditem on prscdt.meditem = meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609)
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            and date(ovst.vstdttm) = CURRENT_DATE - 1
        `);
        return data[0];
    }

    async getG6pdToday(db: Knex) {
        // return [{cid:''}]
        let data = await db.raw(`
            select 
            pt.pop_id as cid
            from hi.pt
            inner join hi.persong6pd on pt.hn = persong6pd.hn
            where pt.dthdate = '0000-00-00'
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            group by pt.hn
        `);
        return data[0];
    }

    async getPersonDate(db: Knex,datestart:any,dateend:any) {
        let data = await db.raw(`
            SELECT DISTINCT * FROM (select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join allergy on pt.hn = allergy.hn 
            where date(allergy.entrydate) between '${datestart}' and '${dateend}'
            group by pt.hn
            union 
            select 
            (select hcode from setup limit 1) as hospital_code,
            pt.pop_id as cid ,
            pt.pname as title,
            pt.fname,
            pt.lname 
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn=ovst.vn 
            inner join prscdt on prscdt.prscno=prsc.prscno 
            inner join meditem on prscdt.meditem=meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609
            )
            and date(vstdttm) between '${datestart}' and '${dateend}'
            group by pt.hn) as p 
        `) ;
        return data[0];
    }    

    async getAllergyDate(db: Knex,datestart:any,dateend:any) {
        // แพ้ยา
        // return [{hospital_code:'',drug_name:'',allergy_level_id:'',report_date:'',tmt_id:'',std_code:'',symptom:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            allergy.namedrug as drug_name,
            allergy.allgtype as allergy_level_id,
            allergy.entrydate as report_date,
            meditem.tmtid tmt_id,
            meditem.stdcode as std_code,
            allergy.detail as symptom,
            pt.pop_id as cid
            from 
            pt
            inner join 
            allergy on pt.hn=allergy.hn
            left join 
            meditem on allergy.meditem = meditem.meditem
            where pt.dthdate = '0000-00-00'
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            and date(allergy.entrydate) between '${datestart}' and '${dateend}'
        `);
        return data[0];
    }    

    async getDrugsDate(db: Knex,datestart:any,dateend:any) {
        // return [{hospital_code:'',drug_name:'',tmt_id:'',std_code:'',use_date:'',cid:''}]
        let data = await db.raw(`
            select 
            (select hcode from setup limit 1) as hospital_code,
            meditem.pres_nm as drug_name,
            meditem.tmtid as tmt_id,
            meditem.stdcode as std_code,
            prsc.prscdate as use_date,
            pt.pop_id as cid
            from pt
            inner join ovst on pt.hn = ovst.hn
            inner join prsc on prsc.vn = ovst.vn 
            inner join prscdt on prscdt.prscno = prsc.prscno 
            inner join meditem on prscdt.meditem = meditem.meditem 
            where meditem.tmtid in (
                196258,
                196262,
                196270,
                196289,
                107576,
                107582,
                107595,
                107609)
            and pt.pop_id not in ('9999999999994','1111111111119')
            and pt.ntnlty = 99
            and date(ovst.vstdttm) between '${datestart}' and '${dateend}'
        `);
        return data[0];
    }

}


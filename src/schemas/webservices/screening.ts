const CreateScreeningSchema = {
  body: {
    type: "object",
    additionalProperties: false,
    properties: {
      hospcode: { type: "string", minLength: 5, maxLength: 5 },
      cid: { type: "string", minLength: 13, maxLength: 13 },
      hn: { type: "string", maxLength: 30 },
      seq: { type: "string", maxLength: 32 },
      date_serv: { type: "string", maxLength: 10, minLenght: 10 }, // yyyy-mm-dd
      time_serv: { type: "string", maxLength: 8, minLenght: 8 }, // hh:mm:ss
      bpd: { type: "number" },
      bps: { type: "number" },
      dtx: { type: "number" },
      oxygen: { type: "number" },
      bmi: { type: "number" },
      weight: { type: "number" },
      height: { type: "number" },
      waist: { type: "number" },
      smoke: { type: "string", enum: ["1", "2", "3", "4", "9"] },
      alcohol: { type: "string", enum: ["1", "2", "3", "4", "9"] },
      depression_q1: { type: "string", enum: ["Y", "N"] },
      depression_q2: { type: "string", enum: ["Y", "N"] },
      depression_q3: { type: "string", enum: ["Y", "N"] },
      ref_id: { type: "string" },
      station_id: { type: "string" },
    },
    required: [
      "hospcode",
      "cid",
      "hn",
      "seq",
      "date_serv",
      "time_serv",
      "bpd",
      "bps",
      "weight",
      "height",
    ]
  }
}

export { CreateScreeningSchema } 